import json
import sqlite3
import logging
import sys

def clean_values(values):
    strs = map(str, values)
    strs = map(lambda x: x.replace("\"", "\"+CHAR(34)+\""), strs)
    strs = map(lambda x: "\"" + x + "\"", strs)
    return ",".join(strs)

def clean_column_names(names):
    strs = [x+"_" if x == "unique" else x for x in names]
    return ",".join(strs)

def initialize_db(conn, dump):
    log = ""
    for k,v in dump.items():
        try:
            sample = v[0]
        except IndexError:
            continue
        table_names = clean_values(sample.keys())
        query = f"CREATE TABLE {k}({table_names})"
        logging.debug(query)
        conn.execute(query)
        for e in v:
            values = clean_values(e.values()) 
            query = f"INSERT INTO {k} VALUES ({values})"
            logging.debug(query)
            conn.execute(query)

if __name__ == "__main__":
    logging.basicConfig(filename="dblog.log", encoding="utf-8", level=logging.DEBUG)
    if len(sys.argv) != 2:
        logging.error("Expected only one argument: the path to the dump.json file")
        exit()
    filename = sys.argv[1]
    with open(filename) as file:
        data = json.load(file)
    with sqlite3.connect("wh.db") as conn:
        initialize_db(conn, data)
